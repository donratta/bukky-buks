
var Event = require('../models/Event'),
 uuid = require('uuid'),
    EventPicture = require('../models/EventPicture')
 TwitterStreamEvent = require('../models/TwitterStreamEvent'),
 EventNote = require('../models/EventNote'),
    InstagramStreamEvent = require('../models/InstagramStreamEvent')  ;
var exec = require('child_process').exec,
    child;

var zipFolder = require('zip-folder');
var Download = require('download');
var Q = require('q');
var zipFolder = require('zip-folder');



exports.create = function(req,res){
    res.render('event/create-event',{title:"Create Event"})
}


exports.createEvent = function(req,res){
    Event.create(req.body,function(err,result){
        if(err){

        }
        //create the twitter and Instagram feed
        var newTwitterObject = {};
        newTwitterObject.eventId = result.id;
        newTwitterObject.status = "STOPPED";
        TwitterStreamEvent.create(newTwitterObject,function(err,twitterStream){
            //then create the Instagram one too
            InstagramStreamEvent.create(newTwitterObject,function(err,igStream){
                res.redirect('/event/list')
            })

        })
    })
}

exports.list = function(req,res){

    Event.find({},function(err,result){
        res.render('event/events',{title:"Events",events:result})
    })


}



exports.getTwitterStreamPage = function(req,res){
      var eventId = req.params.id;
       TwitterStreamEvent.findOne({eventId:eventId},function(err,result){
           if(err){
               console.log(err);
           }
           res.render('event/twitter-stream',{title:"Twitter Stream",twitterStream:result})

       })
}

exports.putTwitterStream = function(req,res){
     var eventId = req.params.id;
    TwitterStreamEvent.findOne({eventId:eventId},function(err,data){
        data.hashtagOne = req.body.hashtagOne? req.body.hashtagOne:"";
        data.hashtagTwo = req.body.hashtagTwo? req.body.hashtagTwo:"";
        data.hashtagThree = req.body.hashtagThree? req.body.hashtagThree:"";
        data.hashtagFour = req.body.hashtagFour? req.body.hashtagFour:"";

        data.save(function(err,result){
            //check here if there is process running.. kill the process then restart it
            TwitterStreamEvent.findOne({eventId:eventId},function(err,result){
                if(!result){
                    res.redirect('/event/'+eventId+'/stream');
                    return;
                }
                if(!result.processId){
                    res.redirect('/event/'+eventId+'/stream');
                    return;
                }
                var processId =  result.processId;
                child = exec('kill'+processId,
                    function (error, stdout, stderr) {
                        result.processId = "";
                        result.status="STOPPED"
                        result.save(function(err){
                            //restart the process
                           var  child2 = exec('node /home/bukky/bukky-buks/twitter-crawler/app.js '+eventId,
                                function (error, stdout, stderr) {
                                    if (error !== null) {
                                        console.log('exec error: ' + error);
                                    }
                                    res.redirect('/event/'+eventId+'/stream') ;
                                });
                        })

                    });
            })



        })

    })
}

exports.restartTwitterStream = function(req,res){
    var eventId = req.params.id;
    TwitterStreamEvent.findOne({eventId:eventId},function(err,result){
        if(!result){
            res.redirect('/event/'+eventId+'/eventPictures');
            return;
        }
        if(!result.processId){
            res.redirect('/event/'+eventId+'/eventPictures');
            return;
        }
        var processId =  result.processId;
        child = exec('kill'+processId,
            function (error, stdout, stderr) {
                result.processId = "";
                result.status="STOPPED"
                result.save(function(err){
                    //restart the process
                    var  child2 = exec('node /home/bukky/bukky-buks/twitter-crawler/app.js '+eventId,
                        function (error, stdout, stderr) {
                            if (error !== null) {
                                console.log('exec error: ' + error);
                            }
                            res.redirect('/event/'+eventId+'/eventPictures') ;
                        });
                })

            });
    })
}


exports.startTwitterStream = function(req,res){
    var eventId = req.params.id;


    child = exec('node /home/bukky/bukky-buks/twitter-crawler/app.js '+req.params.id,
        function (error, stdout, stderr) {
            //console.log('stdout: ' + stdout);
            //console.log('stderr: ' + stderr);

            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });

    res.redirect('/event/'+eventId+'/stream');

}

exports.deactivateTwitterStream = function(req,res){
    var eventId = req.params.id;
    TwitterStreamEvent.findOne({eventId:eventId},function(err,result){
        if(!result){
           res.redirect('/event/list');
            return;
        }
       if(!result.processId){
           res.redirect('/event/'+eventId+'/stream');
           return
       }
       var processId =  result.processId;
        child = exec('kill'+processId,
            function (error, stdout, stderr) {
                result.processId = "";
                result.status="STOPPED"
                result.save(function(err){
                    res.redirect('/event/'+eventId+'/stream') ;

                })

            });

    })
}

exports.listEventPictures= function(req,res){
    var eventId = req.params.id;
    EventPicture.find({eventId:eventId},function(err,result){
        res.render('event/event-pic-list',{pictures:result,eventId:eventId})
    })

}


exports.approvePicture = function(req,res){
    var pictureId = req.params.id;
    EventPicture.findById(pictureId,function(err,result){
        result.status = "APPROVED";
        result.save(function(err,result){
           res.redirect('/event/'+result.eventId+'/eventPictures')
        })

    })

}

exports.approvePictureRest = function(req,res){
    var pictureId = req.params.id;
    EventPicture.findById(pictureId,function(err,result){
        if(result){
            result.status = "APPROVED";
            result.save(function(err,result2){
                EventPicture.find({eventId:result.eventId},function(err,result){
                    res.json(200,result);
                })
            })
        }
        else{
           res.json(200,{});
        }


    })
}
exports.unApprovePicture = function(req,res){
    var pictureId = req.params.id;
    EventPicture.findById(pictureId,function(err,result){
        result.status = "UNAPPROVED";
        result.save(function(err,result){
           res.redirect('/event/'+result.eventId+'/eventPictures')
        })

    })

}

exports.launchPresentation = function(req,res){
    var eventId = req.params.id;
    TwitterStreamEvent.findOne({eventId:eventId},function(err,stream){
        EventPicture.find({eventId:eventId,status:'APPROVED'}).sort({ createdAt : -1}).exec(function(err, results){
            res.render('event/pic-slide-show',{title:"Event Slideshow",pictures:results,hashTag:stream.hashtagOne});
        });
    })
}

exports.listEventPicturesRest = function(req,res){
    var eventId = req.params.id;
    EventPicture.find({eventId:eventId},function(err,result){
        res.json(200,result);
    })

}

exports.unApprovePictureRest = function(req,res){
    var pictureId = req.params.id;
    EventPicture.findById(pictureId,function(err,result){
        result.status = "UNAPPROVED";
        result.save(function(err,result2){
            EventPicture.find({eventId:result.eventId},function(err,result){
                res.json(200,result);
            })
        })

    })

}

exports.rejectPicture = function(req,res){
    var pictureId = req.params.id;
    EventPicture.findById(pictureId,function(err,result){
        if(result){
            result.remove(function(err){
                res.json(200,{});
            });
        }
        else{
            res.json(200,{});
        }


    })
}

exports.deleteEvent = function(req,res){
    var eventId = req.params.id;
    Event.findById(eventId,function(err,result){
        if(result){
            result.remove(function(err){
                res.redirect('/event/list');
                return;
            })
        }
        else{
            res.redirect('/event/list');
        }

    })

};

var majorCount = 0;
var counter;
var saveList;
var downloadAndSave = function(pic,folderName,res){
   // var d = Q.defer();

    new Download({mode: '755'})
        .get(pic.media)
        .dest('/tmp/'+folderName)
        .run(function(err,files){
            if(err){
                console.log('error',err);
            }

            if(counter==(majorCount)){
                child = exec("cd /tmp/"+folderName+";rename 's/\.2$/.jpg/' *.2"+";rename 's/\.2.l$/.jpg/' *.2.l",
                    function (error, stdout, stderr) {
                        if (stderr){
                            console.log(stderr);

                        }
                        zipFolder('/tmp/'+folderName, '/tmp/'+folderName+'.zip', function(err) {
                            if(err) {
                                console.log('oh no!', err);
                            } else {
                                res.download('/tmp/'+folderName+'.zip');
                            }
                        });
                    })
            }
            else{

                counter++;
                downloadAndSave(saveList[counter],folderName,res);
            }

        });



    // return d.promise
};

exports.endEvent = function(req,res){
    //create the folder for the event
    //download the files that have been approved to the folder
    //zip the folder
    //push the zipped folder down to the client
    var eventId = req.params.id;
    var folder = eventId+uuid.v4();
    TwitterStreamEvent.findOne({eventId:eventId},function(err,stream){
        EventPicture.find({eventId:eventId,status:'APPROVED'},function(err,results){
            //results is the event pictures
            child = exec('mkdir /tmp/'+folder,
                function (error, stdout, stderr) {
                            counter = 0;
                            majorCount = results.length;
                            if(majorCount>0){
                                saveList = results;
                                downloadAndSave(saveList[counter],folder,res)

                            }
                    });
                });
        })
}


exports.showEventNotes = function(req,res){
    var eventId = req.params.id;
    EventNote.find({eventId:eventId},function(err,notes){
        res.render('event/event-notes',{title:"Event Notes",eventId:eventId,notes:notes});
    })
}

exports.createEventNote = function(req,res){
    var eventId = req.params.id;
    var note = req.body.noteText;

    var newEventNote = {}
    newEventNote.noteText = note;
    newEventNote.eventId = eventId;
    EventNote.create(newEventNote,function(err,newNote){
        //redirect to the list of the event notes here
        res.redirect('/eventNote/'+eventId);
    })
}

exports.deleteEventNote = function(req,res){
    var noteId = req.params.id;
    var eventId = null;
    EventNote.findById(noteId,function(err,note){
        if(note){
            eventId = note.eventId;
            note.remove(function(err){
                //redirect back to the list of event notes here
                res.redirect('/eventNote/'+eventId);
            })
        }
    })

}












