var request = require('request');
var instagramToken = require('../models/instagramToken');

exports.checkIfUserAuthenticated = function(req,res,next){
    if (req.user) {
        next();
    } else {
        res.redirect('/login');
    }
};

exports.instagramSignIn = function (req, res) {
  var instagramCode = req.query.code;
    console.log('IG CODE:',instagramCode);
    request.post({url: 'https://api.instagram.com/oauth/access_token',
      form: {client_id:'0773c12c402f46b4b9547c5a3503e9c8',
            client_secret: 'e540a75ab76d4f6bbc31bbcee5e38c94',
            grant_type: 'authorization_code',
            redirect_uri: 'http://medirec.com.ng:7000/instagram/oauth',
            code: instagramCode
            }},
      function (error, response, body) {
          if(error){
              console.log(error);
          }
        if (!error && response.statusCode == 200) {
            var jsonBody = JSON.parse(body);
            console.log(jsonBody.access_token);
            // persist this to DB
            instagramToken.findOne({ id: 'main'}, function(err, result){
              if (err) {
                console.error("Error", err);
              }
              if (!result) {
                // create one here
                var newObject = {
                  id: 'main',
                  token: jsonBody.access_token
                }
                instagramToken.create(newObject, function(){
                  console.log("got here:", newObject);
                  res.render('oauth-success',{name: jsonBody.user.full_name});
                })
              }
              else {
                result.token = jsonBody.access_token;
                instagramToken.update({id: 'main'}, { id: 'main', token: jsonBody.access_token});
                console.log("got here: 2");
                res.render('oauth-success',{name: jsonBody.user.full_name});
              }
            })
        }
    })

};