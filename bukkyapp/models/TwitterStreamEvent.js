var mongoose = require('../config/mongo');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var TwitterStreamEvent = mongoose.model('TwitterStreamEvent',
    {
        hashtagOne:String,
        hashtagTwo:String,
        hashtagThree:String,
        hashtagFour:String,
        processId:String,
        status:String,
        eventId:ObjectId,
        createdAt:{ type: Date, default: Date.now }

    })


module.exports =  TwitterStreamEvent;