var mongoose = require('../config/mongo');

var Event = mongoose.model('Event',
    { eventName: String,
      eventAddress:String,
      eventPhone:String,
      eventDate:Date,
      status:{type:String,default:"progress"},
      createdAt:{ type: Date, default: Date.now }
    });



module.exports = Event;