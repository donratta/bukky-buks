var mongoose = require('../config/mongo');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var EventNote = mongoose.model('EventNote',
    {   noteText:String,
        eventId:ObjectId,
        createdAt:{ type: Date, default: Date.now }
    });

module.exports = EventNote;