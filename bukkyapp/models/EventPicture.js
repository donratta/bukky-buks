var mongoose = require('../config/mongo');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var EventPicture = mongoose.model('EventPicture',
    { username: String,
        mediaType:String,
        caption:String,
        objectPath:String,
        profilePic:String,
        eventId:ObjectId,
        media:String,
        twitterMedia:Array,
        status:String,
        createdAt:{ type: Date, default: Date.now }
    });

module.exports = EventPicture;