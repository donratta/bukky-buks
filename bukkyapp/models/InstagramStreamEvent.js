var mongoose = require('../config/mongo');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var InstagramStreamEvent = mongoose.model('InstagramStreamEvent',
    {
        hashtagOne:String,
        hashtagTwo:String,
        hashtagThree:String,
        hashtagFour:String,
        processId:String,
        status:String,
        eventId:ObjectId,
        createdAt:{ type: Date, default: Date.now }
    });



module.exports = InstagramStreamEvent;