
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes'),
 PASSPORT = require('passport'),
    passport = require ('./config/passport')
  , user = require('./routes/user'),
    User = require('./models/User')
  , http = require('http')
  , path = require('path')
    ,eventRoute = require('./routes/event'),

    Auth = require('./routes/authentication')

var app = express();

// all environments
app.set('port', process.env.PORT || 7000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.cookieParser() );
app.use(express.session({secret:'thisismysupersecret'}));
app.use(PASSPORT.initialize());
app.use(PASSPORT.session());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users',Auth.checkIfUserAuthenticated, user.list);
app.get('/event/create',Auth.checkIfUserAuthenticated,eventRoute.create);
app.get('/event/list',Auth.checkIfUserAuthenticated,eventRoute.list);
app.post('/event',Auth.checkIfUserAuthenticated,eventRoute.createEvent);
app.get('/event/:id/stream',Auth.checkIfUserAuthenticated,eventRoute.getTwitterStreamPage)
app.post('/event/:id/twitterStream',Auth.checkIfUserAuthenticated,eventRoute.putTwitterStream)
app.get('/event/:id/deActivateTwitterStream',Auth.checkIfUserAuthenticated,eventRoute.deactivateTwitterStream)
app.get('/event/:id/activateTwitterStream',Auth.checkIfUserAuthenticated,eventRoute.startTwitterStream);
app.get('/event/:id/eventPictures',Auth.checkIfUserAuthenticated,eventRoute.listEventPictures);
app.get('/eventPicture/:id/approve',Auth.checkIfUserAuthenticated,eventRoute.approvePicture);
app.get('/eventPicture/:id/unApprove',Auth.checkIfUserAuthenticated,eventRoute.unApprovePicture);
app.get('/event/:id/launchPresentation',Auth.checkIfUserAuthenticated,eventRoute.launchPresentation);
app.get('/rest/eventPicture/:id/approve',Auth.checkIfUserAuthenticated,eventRoute.approvePictureRest);
app.get('/rest/eventPicture/:id/unApprove',Auth.checkIfUserAuthenticated,eventRoute.unApprovePictureRest);
app.get('/rest/eventPicture/:id/reject',Auth.checkIfUserAuthenticated,eventRoute.rejectPicture);
app.get('/rest/event/:id/listEventPictures',Auth.checkIfUserAuthenticated,eventRoute.listEventPicturesRest);
app.get('/event/:id/delete',Auth.checkIfUserAuthenticated,eventRoute.deleteEvent);
app.post('/eventNote/:id',Auth.checkIfUserAuthenticated,eventRoute.createEventNote);
app.get('/eventNote/:id',Auth.checkIfUserAuthenticated,eventRoute.showEventNotes);
app.get('/eventNote/:id/delete',Auth.checkIfUserAuthenticated,eventRoute.deleteEventNote);
app.get('/event/:id/endEvent',Auth.checkIfUserAuthenticated,eventRoute.endEvent);
app.get('/event/:id/restartSearch',eventRoute.restartTwitterStream);
app.get('/instagram/oauth',Auth.instagramSignIn);
app.post('/login',
    passport.authenticate('local', { successRedirect: '/event/list',
        failureRedirect: '/login',
       })
)
app.get('/login',function(req,res){
    res.render('login')
})
app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/login');
});

app.get('/changePassword',Auth.checkIfUserAuthenticated,function(req,res){
    res.render('change-password');
});

app.post('/changePassword',Auth.checkIfUserAuthenticated,function(req,res){
    //check if the old password is right
    //check if the new passwords match
    //if the new passwords match then change password and log the user out.
    var userSignedIn = req.user;
    if(req.body.oldPassword != userSignedIn.password){
        if(req.body.newPassword==req.body.newPasswordConfirm){
           //change the password and log the user out
            User.findById(userSignedIn._id,function(err,user){
                user.password = req.body.newPassword;
                user.save(function(err,newUser){
                    req.logout;
                    res.redirect('/login');
                })

            })

        }
        else{
            res.redirect('/');
        }
    }
    else{
        res.redirect('/');
    }

});


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
