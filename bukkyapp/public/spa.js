angular.module('bukkyApp', [])
    .controller("EventPictureController",["$http","$scope", function(http,scope) {
        scope.EventPic = {};
        scope.EventPic.pics = null;
        scope.EventPic.eventId = null;
        (scope.EventPic.getAllPics = function(){
           var xx = window.location.href;
            var y = xx.split('/');
            scope.EventPic.eventId = y[4]
            console.log(scope.EventPic.eventId);
            http.get('/rest/event/'+scope.EventPic.eventId+'/listEventPictures')
              .success(function(data,status){
                  console.log(data);
                  scope.EventPic.pics = data;
              })
              .error(function(data,status){
                toastr.error("Cannot get the server:"+status);
              })

        })();

        scope.EventPic.approvePic = function(id,index){
            http.get('/rest/eventPicture/'+id+'/approve')
                .success(function(data,status){
                    scope.EventPic.pics[index].status = "APPROVED"
                    toastr.success("Picture successfully approved")
                    if(scope.EventPic.pics[index].selected){
                        scope.EventPic.pics[index].selected=false;
                    }
                })
        }
        scope.EventPic.unApprovePic = function(id,index){
            http.get('/rest/eventPicture/'+id+'/unApprove')
                .success(function(data,status){
                    scope.EventPic.pics[index].status = "UNAPPROVED";
                    toastr.success("Picture successfully un approved");
                    if(scope.EventPic.pics[index].selected){
                        scope.EventPic.pics[index].selected=false;
                    }
                })
        }
        scope.EventPic.rejectPic = function(id,index){
            http.get('/rest/eventPicture/'+id+'/reject')
                .success(function(data,status){
                    if(scope.EventPic.pics[index].selected){
                        scope.EventPic.pics[index].selected=false;
                    }
                    scope.EventPic.pics.splice(index,1);
                })
        }

        scope.EventPic.filterValue = "ALL";
        scope.filterResult = function(filterValue){
            scope.EventPic.filterValue = filterValue;
        }


        scope.massApprove = function(){
            //go through the list and the ones that have been selected then you push the request up to the s
            //and then set the flag to not selected
           for(var i=0;i<scope.EventPic.pics.length;i++){
              if(scope.EventPic.pics[i].selected){
                  console.log(scope.EventPic.pics[i])
                  scope.EventPic.approvePic(scope.EventPic.pics[i]._id,i);

              }
           }
        }
        scope.massUnApprove =function(){
            //go through the list and the ones that have been seleted then push the request up to the server
            //and then set the flag to not selected
            for(var i=0;i<scope.EventPic.pics.length;i++){
                if(scope.EventPic.pics[i].selected){
                    scope.EventPic.unApprovePic(scope.EventPic.pics[i]._id,i);
                }
            }
        }

        scope.massRejectPicture = function(){
            for(var i=0;i<scope.EventPic.pics.length;i++){
                if(scope.EventPic.pics[i].selected){
                    scope.EventPic.rejectPic(scope.EventPic.pics[i]._id,i);
                }
            }
        }

    }]);