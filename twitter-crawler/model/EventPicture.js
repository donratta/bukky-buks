var mongoose = require('../config/mongo-setting');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var EventPicture = mongoose.model('EventPicture',
    { username: String,
        mediaType:String,
        caption:{type:String},
        objectPath:String,
        profilePic:String,
        eventId:ObjectId,
        media:{type:String},
        twitterMedia:Array,
        status:String,
        createdAt:{ type: Date, default: Date.now }
    });

module.exports = EventPicture;