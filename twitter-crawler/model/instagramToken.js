var mongoose = require('../config/mongo-setting');
var Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

var User = mongoose.model('Instagram',
  {
    token: {
      type: String,
      required: true
    },
    id: String
  }
);

module.exports = User;