

//get the ID of the
var id = process.pid;
console.log(id);
var streamId = process.argv[2] ;
var axios = require('axios');
console.log(streamId);


var twit=require('./config/twitter-credentials');
var TwitterStreamEvent = require('./model/TwitterStreamEvent');
var instagramToken = require('./model/instagramToken');
var EventPicture = require('./model/EventPicture');

var ig = require('instagram-node').instagram();
var access_token = null;

instagramToken.findOne({id: 'main'}, function(err,data){
    console.log(data)
    if(data){
        console.log(data);
        access_token = data.token;
        ig.use({ access_token: data.token});
    }

});

TwitterStreamEvent.findOne({eventId:streamId},function(err,data){
    if(!data){
        //then end the process it should exist
        console.log("no data");
        process.exit();
    }
    else{
        data.status = "RUNNING"

        startTwitterSearch(data);
        var search = [];
        if(data.hashtagOne){
            search.push('#'+data.hashtagOne)
        } if(data.hashtagTwo){
            search.push('#'+data.hashtagTwo)
        } if(data.hashtagThree){
            search.push('#'+data.hashtagThree)
        }if(data.hashtagFour){
            search.push('#'+data.hashtagFour)
        }

        data.processId = id;

        data.save(function(err,res){

        })

        twit.stream('statuses/filter',{track:search,language:"en",filter:['picture']},function(stream){
            stream.on('data',function(data){
                //console.log(data.entities.media)
                //console.log(data.entities.media)

                if(!data.entities.media || !data.entities.media[0].media_url)
                    return;

                var newEventPicture = {};
                newEventPicture.caption = data.text;
                newEventPicture.profilePic = data.user.profile_image_url;
                newEventPicture.eventId = streamId;
                newEventPicture.status= "UNAPPROVED";
                newEventPicture.username = data.user.screen_name;
                newEventPicture.mediaType = "TWITTER";
                newEventPicture.twitterMedia = data.entities.media;
                if(data.entities.media)
                newEventPicture.media = data.entities.media[0].media_url;
                EventPicture.create(newEventPicture,function(err,pic){
                    console.log("pic created")

                })


            })
            stream.on('error',function(data){
                console.log(data);
            })

        });


    }



})

var igCallback = function(err, medias, pagination, remaining, limit) {
    // Your implementation here
    checkAndInsertDataRecursive(medias,0);
    if(pagination.next) {
        pagination.next(igCallback); // Will get second page results
    }
};
var hashTagMinOne;
var hashTagMinTwo;
var hashTagMinThree;
var hashTagMinFour;
var startTwitterSearch = function(data){
    setInterval(function(){
        if(data.hashtagOne){
            console.log('Hastag one:', data.hashtagOne)
            newInstagram(data.hashtagOne);
            // ig.tag_media_recent(data.hashtagOne, function(err, medias, pagination, remaining, limit) {
            //     console.log('err:', err);
            //     console.log(medias)
            //     if(medias && medias.length>0)
            //     checkAndInsertDataRecursive(medias,0);
            //     if(pagination.next) {
            //         console.log("next");
            //         pagination.next(igCallback); // Will get second page results
            //     }
            // });
        }
        if(data.hashtagTwo){
            console.log('hastag two:', data.hashtagTwo);
            // ig.tag_media_recent(data.hashtagTwo, function(err, medias, pagination, remaining, limit) {
            //     console.log('error:', err);
            //     console.log(medias);
            //     if(medias && medias.length>0)
            //     checkAndInsertDataRecursive(medias,0);
            //     if(pagination.next) {
            //         pagination.next(igCallback); // Will get second page results
            //     }
            // });
            newInstagram(data.hashtagTwo);
        }
        if(data.hashtagThree){

            // ig.tag_media_recent(data.hashtagThree, function(err, medias, pagination, remaining, limit) {
            //     console.log('error:', err);
            //     console.log(medias);
            //     if(medias && medias.length>0)
            //     checkAndInsertDataRecursive(medias,0);
            //     if(pagination.next) {
            //         pagination.next(igCallback); // Will get second page results
            //     }
            //
            // });
        }
        if(data.hashtagFour){
            // ig.tag_media_recent(data.hashtagFour, function(err, medias, pagination, remaining, limit) {
            //     console.log('error:', err);
            //     console.log(medias);
            //     if(medias.length>0)
            //     checkAndInsertDataRecursive(medias,0);
            //     if(pagination.next) {
            //         pagination.next(igCallback); // Will get second page results
            //     }
            // });
        }
    },15000)
}



function checkAndInsertData(post){
    if(!post){
        return;
    }
    if(post.type!='image'){
        return;
    }
    //insert the image into database here and massage the data here


    var newEventPicture = {};
    newEventPicture.caption = post.caption.text;
    newEventPicture.profilePic = post.user.profile_picture;
    newEventPicture.eventId = streamId;
    newEventPicture.status= "UNAPPROVED";
    newEventPicture.username = post.user.username;
    newEventPicture.mediaType = "INSTAGRAM";
    newEventPicture.media = post.images.standard_resolution.url;

    EventPicture.findOne({media:post.images.standard_resolution.url,eventId:streamId},function(err,results){
            if(!results){
                EventPicture.create(newEventPicture,function(err,pic){
                    console.log(err);

                })
            }
    })

    //insert the data into the database here

}


var checkAndInsertDataRecursive = function(posts,indexValue){
    if(indexValue == (posts.length-1)){
        return;
    }
    else{
        var post = posts[indexValue];
        if(!post){
            return;
        }
        if(post.type!='image'){
            return;
        }
        var newEventPicture = {};
        newEventPicture.caption = post.caption.text;
        newEventPicture.profilePic = post.user.profile_picture;
        newEventPicture.eventId = streamId;
        newEventPicture.status= "UNAPPROVED";
        newEventPicture.username = post.user.username;
        newEventPicture.mediaType = "INSTAGRAM";
        newEventPicture.media = post.images.standard_resolution.url;
        EventPicture.findOne({media:post.images.standard_resolution.url,eventId:streamId},function(err,results){
            if(!results){
                EventPicture.create(newEventPicture,function(err,pic){
                    indexValue++;
                    return checkAndInsertDataRecursive(posts,indexValue);
                })
            }
        })
    }
}


var newInstagram = function (hashTag) {
    const url = `https://api.instagram.com/v1/tags/${hashTag}/media/recent?access_token=${access_token}`;
    axios.get(url)
      .then(function(response){
          console.log(response.data);
      })
      .catch(function(err) {
          console.log('Error', err);
      })


}
